package me.johnweland.recipedemo;

/**
 * Created by jweland on 4/15/2016.
 */
public class DirectionsFragment extends CheckboxesFragment {
    @Override
    public String[] getContents(int index) {
        return Recipes.directions[index].split("`");
    }
}
